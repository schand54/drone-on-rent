import axios from 'axios';
import history from '../history';

console.log(process.env.REACT_APP_URL);

export const _api = axios.create({
    baseURL: process.env.REACT_APP_URL,
});

_api.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        if (error.response && error.response.status) {
            switch (error.response.status) {
                case 404:
                    history.push('/404');
                    break;
                default: {
                    console.log(error);
                }
            }
        }
        return Promise.reject(error);
    }
)