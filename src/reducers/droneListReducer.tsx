import { GET_DRONE_LIST, SET_DRONE_LIST } from '../actions/actionTypes';
import { Action } from '../actions/interfaces';

const initialState: { droneList: any[] | null } = {
    droneList: [[], [], []]
};

export default (state = initialState, action: Action) => {
    switch (action.type) {
        case GET_DRONE_LIST:
            if (action.payload && action.payload.length) {
                return { ...state, droneList: [...action.payload] };
            }
            return { ...state };
        case SET_DRONE_LIST:
            if (action.payload && action.payload.length) {
                return { ...state, droneList: [...action.payload] };
            }
            return { ...state };
        default:
            return { ...state };
    }
}