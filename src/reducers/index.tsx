import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import userListReducer from './userListReducer';
import droneListReducer from './droneListReducer';

export default combineReducers({
    user: userListReducer,
    drone: droneListReducer,
    form: formReducer
});