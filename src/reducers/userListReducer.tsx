import {
    GET_USER_LIST,
    SET_USER_LIST,
    SET_CURRENT_USER,
    REMOVE_CURRENT_USER,
    GET_CURRENT_USER
} from '../actions/actionTypes';
import { Action } from '../actions/interfaces';
import {User} from '../components/features/login/interfaces';

const initialState: { userList: User[] | null, currentUser: User | null } = {
    userList: [],
    currentUser: null
};

export default (state = initialState, action: Action) => {
    switch (action.type) {
        case GET_USER_LIST:
            if (action.payload && action.payload.length) {
                return { ...state, userList: [...action.payload] };
            }
            return { ...state };
        case SET_USER_LIST:
            if (action.payload && action.payload.length) {
                return { ...state, userList: [...action.payload] };
            }
            return { ...state };
        case SET_CURRENT_USER:
            console.log('in reducer : ', action);
            if (action.payload) {
                return { ...state, currentUser: action.payload };
            }
            return { ...state };
        case GET_CURRENT_USER:
            console.log('in reducer : ', action);
            if (action.payload) {
                return { ...state, currentUser: action.payload };
            }
            return { ...state };
        case REMOVE_CURRENT_USER:
            return { ...state, currentUser: null };
        default:
            return { ...state };
    }
}