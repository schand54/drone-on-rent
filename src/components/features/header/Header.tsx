import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import styles from './Header.module.scss';
import { setCurrentUser, removeCurrentUser } from '../../../actions';
import { AppState } from '../../../AppState';
import { HeaderProps } from './interfaces';
import Modal from '../../shared/modal/Modal';
import { ToastContainer, toast } from 'react-toastify';

class Header extends React.Component<HeaderProps, { showModal: boolean }> {
    
    constructor(props: HeaderProps) {
        super(props);
        this.state = { showModal: false };
    }

    onLogin = (email: string) => {
        if (!email) {
            this.noUser('Invalid email');
            console.log('heaer log in : ', email);
        } else {
            let setUser = false;
            if (this.props.userList) {
                const user = this.props.userList.filter(el => el.email === email)[0];
                if (user && user.email) {
                    setUser = true;
                    this.props.setCurrentUser(email);
                }
            }
            if (!setUser) {
                this.noUser('User does not exists. Please register first');
            }
            this.setState({ showModal: false });
        }
    }

    noUser = (message: string) => toast.warn(message, {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
    });

    handleBtnClick = () => {
        if (this.props.currentUser) {
            this.props.removeCurrentUser();
        } else {
            this.setState({ showModal: true });
        }
    }

    render() {
        return (
            <div className={styles.Header}>
                <div className={styles.Header__Heading}>
                    <Link to="/" className={styles.Header__Heading__Logo}>
                        <img src="/images/favicon.png" alt="brand" />
                        <span className={styles.Header__Heading__Logo__Text}>
                            AWOK
                        </span>
                    </Link>
                </div>
                <div className={styles.Header__Content}>
                    <ul className={styles.Header__Content__List}>
                        <li className={styles.Header__Content__List__Item}>
                            <Link to="/drones">Explore Drones</Link>
                        </li>
                    </ul>
                </div>
                <div className={styles.Header__Auth}>
                    <button
                        className={styles.Header__Auth__Btn}
                        style={{ background: this.props.currentUser ? 'crimson' : 'dodgerblue' }}
                        onClick={this.handleBtnClick}
                    >
                        {this.props.currentUser && this.props.currentUser.email ? 'Logout' : 'Login'}
                    </button>
                </div>
                {this.state.showModal ?
                    <Modal
                        onLogin={this.onLogin}
                        handleClose={() => this.setState({showModal: false})}
                    /> :
                    null
                }
                <ToastContainer />
            </div>
        );
    }
}

const mapStateToProps = (state: AppState) => {
    return {
        currentUser: state.user && state.user.currentUser,
        userList: state.user && state.user.userList
    };
}

const mapDispatchToProps = {
    setCurrentUser,
    removeCurrentUser
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);