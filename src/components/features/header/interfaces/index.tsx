import { User } from "../../login/interfaces";

export interface HeaderProps {
    currentUser: User | null,
    userList: User[] | null,
    setCurrentUser(email: string): void,
    removeCurrentUser(): void
}