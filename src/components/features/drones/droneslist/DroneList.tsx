import React from 'react';
import { connect } from 'react-redux';

import styles from './DroneList.module.scss';
import { AppState } from '../../../../AppState';
import { getDroneList, setDroneList, setCurrentUser, getCurrentUser } from '../../../../actions';
import { DroneListProps } from './interfaces';
import quads from '../../../quads.json';
import Card from '../../../shared/card/Card';
import Modal from '../../../shared/modal/Modal';
import { ToastContainer, toast } from 'react-toastify';

class DroneList extends React.Component<DroneListProps, {showModal: boolean}>{
    stations: any[] = [[], [], []];
    bookingManufacturer = '';
    bookingModel = '';
    bookingFlightTime = '';

    constructor(props: DroneListProps) {
        super(props);
        this.state = { showModal: false };
    }
    
    componentDidMount() {
        const isEmpty = this.checkDroneList();
        if (isEmpty) {
            this.allocateDrones(quads.quads);
            this.props.setDroneList(JSON.parse(JSON.stringify(this.stations)));
        }
    }

    checkDroneList = (): boolean => {
        let isEmpty = true;
        console.log('droneList : ', this.props.droneList);
        if (this.props.droneList) {
            this.props.droneList.forEach(el => {
                if (el.length) {
                    isEmpty = false;
                }
            });
        }
        return isEmpty;
    }

    allocateDrones(drones: any[]) {
        drones.forEach(el => {
            const stationIdx = Math.floor(Math.random() * 2.99);
            if (this.stations[stationIdx] && this.stations[stationIdx].length < 10) {
                this.stations[stationIdx].push(el);
            } else if (this.stations[stationIdx + 1] && this.stations[stationIdx + 1].length < 10) {
                this.stations[stationIdx + 1].push(el);
            } else if (this.stations[stationIdx + 2] && this.stations[stationIdx + 2].length < 10) {
                this.stations[stationIdx + 2].push(el);
            } else if (this.stations[stationIdx - 1] && this.stations[stationIdx - 1].length < 10) {
                this.stations[stationIdx - 1].push(el);
            } else if (this.stations[stationIdx - 2] && this.stations[stationIdx - 2].length < 10) {
                this.stations[stationIdx - 2].push(el);
            }
        });
        console.log('stations : ', this.stations);
    }

    onBookDrone = (manufacturer: string, model: string, flightTime: string) => {
        this.bookingManufacturer = manufacturer;
        this.bookingModel = model;
        this.bookingFlightTime = flightTime;
        this.setState({ showModal: true });
    }

    onSubmitDrone = (manufacturer: string, model: string, email: string) => {
        if (this.props.droneList) {
            const newList = this.props.droneList.map(el => {
                if (el.length) {
                    el.map((subEl: any) => {
                        if (
                            subEl.manufacturer === manufacturer &&
                            subEl.model === model &&
                            subEl.bookedBy === email
                        ) {
                            delete subEl.bookedBy;
                            delete subEl.bookingTime;
                            subEl.isAvailable = true;
                        }
                        return { ...subEl };
                    });
                }
                return el;
            });
            console.log('newList : ', newList);
            this.props.setDroneList(newList);
            this.forceUpdate();
        } else {
            this.errorMessage('No drone found');
        }
    }

    renderDroneItems = (list: any[]) => {
        if (list && list.length) {
            return list.map((el, i) => {
                return (
                    <div className={styles.Drone__List__Items__Item} key={`${el.manufacturer}${el.model}`}>
                        <Card
                            {...el}
                            onBookDrone={this.onBookDrone}
                            currentUserEmail={this.props.currentUser && this.props.currentUser.email}
                            onSubmitDrone={this.onSubmitDrone}
                        />
                    </div>
                );
            });
        }
        return null;
    }

    renderDrones = () => {
        if (this.props.droneList) {
            return this.props.droneList.map((el, i) => {
                return (
                    <div className={styles.Drone__List} key={'station' + (i + 1)}>
                        <div className={styles.Drone__List__Title}>
                            {`Station ${i + 1}`}
                        </div>
                        <div className={styles.Drone__List__Items}>
                            {this.renderDroneItems(el)}
                        </div>
                    </div>
                );
            });
        }
        return <div style={{ minHeight: '90vh'}}></div>;
    }

    setDroneBooking = (email: string) => {
        console.log('setting drone booking');
        if (this.props.droneList) {
            console.log(this.bookingManufacturer, this.bookingModel, this.bookingFlightTime, this.props.droneList);
            const newDroneList = this.props.droneList.map(el => {
                if (el.length) {
                    el = el.map((subEl: any) => {
                        if (
                            subEl.manufacturer === this.bookingManufacturer &&
                            subEl.model === this.bookingModel &&
                            subEl.maxFlightTime === this.bookingFlightTime
                        ) {
                            subEl['bookedBy'] = email;
                            subEl['bookingTime'] = new Date().getTime();
                            subEl['isAvailable'] = false;
                        }
                        return subEl;
                    });
                }
                return el;
            });
            console.log('newDRONE LIST :', newDroneList);
            this.props.setDroneList(newDroneList);
            this.forceUpdate();
        } else {
            this.errorMessage('No drones found');
        }
    }

    checkForBooking = (email: string): boolean => {
        let isDroneAvailable = false;
        let isUserAvailable = true;
        if (this.props.droneList) {
            this.props.droneList.forEach(el => {
                if (el.length) {
                    el.forEach((subEl: any) => {
                        if (
                            subEl.manufacturer === this.bookingManufacturer &&
                            subEl.model === this.bookingModel &&
                            subEl.maxFlightTime === this.bookingFlightTime &&
                            subEl.isAvailable
                        ) {
                            isDroneAvailable = true;
                        }
                        if (subEl.bookedBy && subEl.bookedBy === email) {
                            isUserAvailable = false;
                        }
                    });
                }
            });
            if (isDroneAvailable && isUserAvailable) {
                return true;
            }
        } else {
            this.errorMessage('No drones found');
            return false;
        }
        return false;
    }

    onLogin = (email: string) => {
        console.log('in dronelist onLogin : ', email);
        if (!email) {
            this.warnMessage('Invalid Email');
        } else {
            if (this.props.userList) {
                const user = this.props.userList.filter(el => el.email === email)[0];
                const currentUser = this.props.currentUser || null;
                console.log('fronlist : ', currentUser, user);
                if (user && user.email) {
                    if (currentUser && currentUser.email !== user.email) {
                        this.warnMessage('Please log out first');
                    } else {
                        this.props.setCurrentUser(email);
                        const isBookingAvailable = this.checkForBooking(email);
                        if (isBookingAvailable) {
                            this.setDroneBooking(email);
                        } else {
                            this.errorMessage('Booking not possible');
                        }
                    }
                }
            } else {
                this.errorMessage('User does not exists!!');
            }
            this.setState({ showModal: false });
        }
        
    }

    warnMessage = (message: string) => toast.warn(message, {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
    });

    errorMessage = (message: string) => toast.error(message, {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
    });

    render() {
        return (
            <div className={styles.Drone}>
                {this.renderDrones()}
                {this.state.showModal ?
                    <Modal
                        onLogin={this.onLogin}
                        handleClose={() => this.setState({showModal: false})}
                    /> :
                    null
                }
                <ToastContainer />
            </div>
        );
    }
}

const mapStateToProps = (state: AppState) => {
    return {
        droneList: state.drone && state.drone.droneList,
        userList: state.user && state.user.userList,
        currentUser: state.user && state.user.currentUser
    };
}

const mapDispatchToProps = {
    getDroneList,
    setDroneList,
    setCurrentUser,
    getCurrentUser
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DroneList);