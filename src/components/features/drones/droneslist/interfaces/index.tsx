import {User} from '../../../login/interfaces';

export interface DroneListProps {
    droneList: any[] | null,
    userList: User[] | null,
    currentUser: User | null,
    getDroneList(): void,
    getCurrentUser(): void,
    setDroneList(droneList: any[]): void,
    setCurrentUser(email: string): void
}