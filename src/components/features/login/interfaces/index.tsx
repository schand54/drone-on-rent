export interface User {
    firstName: string,
    lastName: string,
    email: string,
    phone: string
}


export interface LoginProps {
    handleSubmit: any,
    userList: User[] | null,
    formValues: any
    setUserList(userList: User[]): any,
    reset(): void
}