import React from 'react';
import { reduxForm, Field } from 'redux-form'
import {connect} from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import styles from './Login.module.scss';
import { AppState } from '../../../AppState';
import { setUserList } from '../../../actions';
import { User, LoginProps } from './interfaces';

class Register extends React.Component<LoginProps> {
    componentDidMount = () => {
        console.log('login Props ::', this.props);
    }

    userExistsNotification = () => toast.error("User already exists!", {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
    });

    successNotification = (message: string) => toast.success(message, {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
    });

    handleSubmit = (formValues: User) => {
        console.log(formValues, this.props.userList);
        if (this.props.userList) {
            const newList: User[] | null = [...this.props.userList];
            const filteredArr = this.props.userList.filter(el => {
                if (
                    el.firstName.toLowerCase() === formValues.firstName.toLowerCase() &&
                    el.lastName.toLowerCase() === formValues.lastName.toLowerCase() &&
                    el.email.toLowerCase() === formValues.email.toLowerCase() &&
                    el.phone.toLowerCase() === formValues.phone.toLowerCase()
                ) {
                    return true;
                }
                return false;
            });
            if (filteredArr && filteredArr.length) {
                this.userExistsNotification();
            } else {
                newList.push({ ...formValues });
                console.log('newList ::', newList);
                this.props.setUserList(newList);
                this.successNotification('Registered Successfully. Please login to rent drones.');
                this.props.reset();
                // history.push('/drones');
            }
        }
    }

    renderForm = () => {
        return (
            <div className={styles.Login}>
                <h3 className={styles.Login__Title}>Register now</h3>
                <form className={styles.Login__Form} onSubmit={this.props.handleSubmit(this.handleSubmit)}>
                    {/* <label for="username">Username</label> */}
                    <div className={styles.Login__Form__Field}>
                        <div className={styles.Login__Form__Field__Control}>
                            {/* <label className={styles.Login__Form__Field__Control__Label}>
                                First Name
                            </label> */}
                            <Field
                                className={styles.Login__Form__Field__Control__Input}
                                name="firstName"
                                component="input"
                                type="text"
                                autoComplete="off"
                                placeholder="First Name"
                            />
                        </div>
                    </div>
                    <div className={styles.Login__Form__Field}>
                        <div className={styles.Login__Form__Field__Control}>
                            {/* <label className={styles.Login__Form__Field__Control__Label}>
                                Last Name
                            </label> */}
                            <Field
                                className={styles.Login__Form__Field__Control__Input}
                                name="lastName"
                                component="input"
                                type="text"
                                autoComplete="off"
                                placeholder="Last Name"
                            />
                        </div>
                    </div>
                    <div className={styles.Login__Form__Field}>
                        <div className={styles.Login__Form__Field__Control}>
                            {/* <label className={styles.Login__Form__Field__Control__Label}>
                                Email ID
                            </label> */}
                            <Field
                                className={styles.Login__Form__Field__Control__Input}
                                name="email"
                                component="input"
                                type="email"
                                autoComplete="off"
                                placeholder="Email Address"
                            />
                        </div>
                    </div>
                    <div className={styles.Login__Form__Field}>
                        <div className={styles.Login__Form__Field__Control}>
                            {/* <label className={styles.Login__Form__Field__Control__Label}>
                                Phone Number
                            </label> */}
                            <Field
                                className={styles.Login__Form__Field__Control__Input}
                                name="phone"
                                component="input"
                                type="tel"
                                pattern="[0-9]{10}"
                                autoComplete="off"
                                placeholder="Phone Numbers"
                            />
                        </div>
                    </div>
                    <div className={styles.Login__Form__Field}>
                        <div className={styles.Login__Form__Field__Control}>
                            <button className={styles.Login__Form__Field__Control__Submit} type="submit">
                                <span>Register</span>
                            </button>
                        </div>
                    </div>
                </form>
                <ToastContainer />
            </div>
        );
    }
    render = () => {
        return (
            <React.Fragment>
                {this.renderForm()}
            </React.Fragment>
        );
    }
}


const validate = (formValues: any) => {
    const errors: any = {};
    if (!formValues.firstName) {
        errors.title = 'You must enter a first name';
    }
    if (!formValues.lastName) {
        errors.description = 'You must enter a last name';
    }
    if (!formValues.email) {
        errors.description = 'You must enter an email';
    }
    if (!formValues.phone) {
        errors.description = 'You must enter a phone number';
    }
    return errors;
};

const mapStateToProps = (state: AppState) => {
    return {
        formValues: state.form && state.form.registerForm && state.form.registerForm.values,
        userList: state.user && state.user.userList
    };
}

const mapDispatchToProps = {
    setUserList
}

const RegisterConnected = connect(
    mapStateToProps,
    mapDispatchToProps
)(Register);

export default reduxForm({
    form: 'registerForm',
    validate
})(RegisterConnected);