import { User } from '../../login/interfaces';

export interface HomepageProps {
    userList: Array<User> | null,
    droneList: any[] | null,
    getUserList(): void,
    getDroneList(): void,
    setDroneList(droneList: any[]): void,
    getCurrentUser(): void
}