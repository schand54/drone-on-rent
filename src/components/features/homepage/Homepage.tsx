import React from 'react';
import { connect } from 'react-redux';

import styles from './Homepage.module.scss';
import Login from '../login/Login';
import { HomepageProps } from './interfaces';
import { AppState } from '../../../AppState';
import { getUserList, getDroneList, setDroneList, getCurrentUser } from '../../../actions';

class Homepage extends React.Component<HomepageProps> {
    stations: any[] = [[], [], []];
    componentDidMount() {
        console.log('homepage props :', this.props);
        this.props.getUserList();
        this.props.getDroneList();
        this.props.getCurrentUser();
    }

    render() {
        return (
            <div className={styles.Homepage}>
                <div className={styles.Homepage__LoginContainer}>
                    <Login />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: AppState) => {
    console.log('state ::', state);
    return {
        userList: state.user && state.user.userList,
        droneList: state.drone && state.drone.droneList
    };
}

const mapDispatchToProps = {
    getUserList,
    getDroneList,
    setDroneList,
    getCurrentUser
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Homepage);