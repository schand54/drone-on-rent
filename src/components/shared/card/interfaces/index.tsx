export interface CardProps {
    manufacturer: string,
    model: string,
    maxFlightTime: string,
    charge: string,
    img: string,
    isAvailable: boolean,
    bookedBy?: string,
    bookingTime?: number,
    currentUserEmail?: string | null,
    mins?: number,
    secs?: number,
    onBookDrone(manufacturer: string, model: string, maxFlightTime: string): void
    onSubmitDrone(manufacturer: string, model: string, email: string): void
}