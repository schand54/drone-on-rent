import React from 'react';

import styles from './Card.module.scss';
import { CardProps } from './interfaces';

class Card extends React.Component<CardProps, { minutes?: number, secs?: number }> {
    constructor(props: CardProps) {
        super(props);
        this.state = { minutes: 0, secs: 0 };
    }

    componentDidMount = () => {
        if (
            !this.props.isAvailable &&
            this.props.bookingTime &&
            this.props.bookedBy === this.props.currentUserEmail
        ) {
            let bookinTime = this.props.bookingTime;
            const min = +(this.props.maxFlightTime.split('min')[0]);
            bookinTime += min * 1000 * 60;
            console.log('mine :', min);
            const timer = setInterval(() => {
                const now = new Date();
                const diffTime = now.getTime() - bookinTime;
                const diffMinutes = Math.floor((diffTime % (1000 * 60 * 60)) / (1000 * 60));
                const diffSecs = Math.floor((diffTime % (1000 * 60)) / (1000));
                console.log('diffTime', diffMinutes, diffSecs);
                if (diffSecs < 0) {
                    console.log('diffMinutes, diffSecs : ', diffMinutes, diffSecs);
                    this.setState({ minutes: Math.abs(diffMinutes), secs: Math.abs(diffSecs) });
                } else {
                    clearInterval();
                    this.handleBook('Submit');
                }
            }, 1000);
        }
    }

    handleBook = (flag?: string) => {
        if (flag !== 'Submit') {
            this.props.onBookDrone(
                this.props.manufacturer,
                this.props.model,
                this.props.maxFlightTime
            );
        } else if (flag === 'Submit') {
            this.props.onSubmitDrone(
                this.props.manufacturer,
                this.props.model,
                this.props.bookedBy || ''
            );
        }
    }

    renderTimer = () => {
        if (
            !this.props.isAvailable &&
            this.props.bookingTime &&
            this.props.bookedBy === this.props.currentUserEmail
        ) {
            return (
                <div className={styles.Card__Footer__Timer}>
                    {this.state.minutes} : {this.state.secs}
                </div>
            );
        }
        return null;
    }

    renderBookButton = () => {
        let text = 'Book Now';
        if ((this.state.minutes && this.state.minutes > 0) || (this.state.secs && this.state.secs > 0)) {
            text = 'Submit'
        }
        return (
            <button
                className={styles.Card__Footer__Btn}
                onClick={() => this.handleBook(text)}
            >
                {text}
            </button>
        );
    }

    render() {
        return (
            <div className={styles.Card}>
                <div className={styles.Card__Header}>
                    <div className={styles.Card__Header__ImgContainer}>
                        <img src={this.props.img} alt='header' className={styles.Card__Header__ImgContainer__Img}/>
                    </div>
                </div>
                <div className={styles.Card__Body}>
                    <div>
                        <div className={styles.Card__Body__Title}>
                            {this.props.manufacturer}
                        </div>
                        <div className={styles.Card__Body__SubTitle}>
                            {this.props.model}
                        </div>
                    </div>
                    <div>
                        <div className={styles.Card__Body__Charge}>
                            <span>Charged : </span>{this.props.charge}
                        </div>
                    </div>
                </div>
                <div className={styles.Card__Footer}>
                    {(this.state.minutes || this.state.secs) && this.renderTimer()}
                    {this.renderBookButton()}
                </div>
            </div>
        );
    }
}

export default Card;