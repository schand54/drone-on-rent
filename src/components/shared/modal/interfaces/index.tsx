export interface ModalProps {
    handleClose(): void,
    onLogin(email: string): void
}