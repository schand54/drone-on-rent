import React from 'react';
import { Router, Route, Switch } from 'react-router';

import history from '../history';
import Header from './features/header/Header';
import Homepage from './features/homepage/Homepage';
import DroneList from './features/drones/droneslist/DroneList';

class AppRoute extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Router history={history}>
                    <div>
                        <Header />
                        <Switch>
                            <Route path="/" exact component={Homepage} />
                            <Route path="/drones" exact component={DroneList} />
                        </Switch>
                    </div>
                </Router>
            </React.Fragment>
        )
    }
}

export default AppRoute;