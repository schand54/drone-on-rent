import React from 'react';

import AppRoute from './AppRoute';
import styles from './App.module.scss';

class App extends React.Component {

    render() {
        return (
            <React.Fragment>
                <div className={styles.App}>
                    <AppRoute />
                </div>
            </React.Fragment>
        )
    }
}

export default App;