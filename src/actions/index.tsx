import { Dispatch } from 'redux';

import * as actionTypes from './actionTypes';
import { User } from '../components/features/login/interfaces';

export const getUserList = () => (dispatch: Dispatch) =>  {
    const userList = localStorage.getItem('userList');
    let payload = null;
    if (userList) {
        payload = JSON.parse(userList);
    }
    console.log('getUserList : ', payload);
    dispatch({ type: actionTypes.GET_USER_LIST, payload: payload || [] });
}

export const setUserList = (userList: any[]) => (dispatch: Dispatch) => {
    const oldList = localStorage.getItem('userList');
    const newList = JSON.stringify(userList);
    if (oldList !== newList) {
        localStorage.setItem('userList', newList);
    }
    dispatch({ type: actionTypes.SET_USER_LIST, payload: JSON.parse(newList) || [] });
}
export const getDroneList = () => (dispatch: Dispatch) =>  {
    const droneList = localStorage.getItem('droneList');
    let payload = null;
    if (droneList) {
        payload = JSON.parse(droneList);
    }
    console.log('getDroneList : ', payload);
    dispatch({ type: actionTypes.GET_DRONE_LIST, payload: payload || [] });
}

export const setDroneList = (droneList: any[]) => (dispatch: Dispatch) => {
    const oldList = localStorage.getItem('droneList');
    const newList = JSON.stringify(droneList);
    if (oldList !== newList) {
        localStorage.setItem('droneList', newList);
    }
    console.log('new Drones list ::', JSON.parse(newList));
    dispatch({ type: actionTypes.SET_DRONE_LIST, payload: JSON.parse(newList) || [] });
}

export const setCurrentUser = (email: string) => (dispatch: Dispatch) => {
    const userList = localStorage.getItem('userList');
    let user = null;
    if (userList) {
        let list = JSON.parse(userList);
        user = list.filter((el: User) => el && el.email === email)[0];
        console.log(list, user);
        if (user) {
            localStorage.setItem('currentUser', JSON.stringify(user));
        }
    }
    dispatch({ type: actionTypes.SET_CURRENT_USER, payload: user });
}

export const getCurrentUser = () => (dispatch: Dispatch) => {
    const currentUser = localStorage.getItem('currentUser');
    dispatch({ type: actionTypes.GET_CURRENT_USER, payload: currentUser ? JSON.parse(currentUser) : null})
}

export const removeCurrentUser = () => (dispatch: Dispatch) => {
    const user = localStorage.getItem('currentUser');
    if (user) {
        localStorage.removeItem('currentUser');
    }
    dispatch({ type: actionTypes.REMOVE_CURRENT_USER });
}

export const setLoader = () => {
    return { type: actionTypes.SET_LOADER };
}

export const clearLoader = () => {
    return { type: actionTypes.CLEAR_LOADER };
}
