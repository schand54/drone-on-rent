export const SET_PAGE_TITLE = 'SET_PAGE_TITLE';
export type SET_PAGE_TITLE = typeof SET_PAGE_TITLE;

export const SET_LOADER = 'SET_LOADER';
export type SET_LOADER = typeof SET_LOADER;

export const CLEAR_LOADER = 'CLEAR_LOADER';
export type CLEAR_LOADER = typeof CLEAR_LOADER;

export const GET_USER_LIST = 'GET_USER_LIST';
export type GET_USER_LIST = typeof GET_USER_LIST;

export const SET_USER_LIST = 'SET_USER_LIST';
export type SET_USER_LIST = typeof SET_USER_LIST;

export const GET_DRONE_LIST = 'GET_DRONE_LIST';
export type GET_DRONE_LIST = typeof GET_DRONE_LIST;

export const SET_DRONE_LIST = 'SET_DRONE_LIST';
export type SET_DRONE_LIST = typeof SET_DRONE_LIST;

export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export type SET_CURRENT_USER = typeof SET_CURRENT_USER;

export const GET_CURRENT_USER = 'GET_CURRENT_USER';
export type GET_CURRENT_USER = typeof GET_CURRENT_USER;

export const REMOVE_CURRENT_USER = 'REMOVE_CURRENT_USER';
export type REMOVE_CURRENT_USER = typeof REMOVE_CURRENT_USER;