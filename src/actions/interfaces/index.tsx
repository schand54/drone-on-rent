import * as actionTypes from '../actionTypes';
import {User} from '../../components/features/login/interfaces';

// Action Interfaces
export interface SetPageTitle {
    type: actionTypes.SET_PAGE_TITLE,
    payload: string
}

export interface SetLoader {
    type: actionTypes.SET_LOADER
}

export interface ClearLoader {
    type: actionTypes.CLEAR_LOADER
}

export interface GetUserList {
    type: actionTypes.GET_USER_LIST,
    payload: null | User[]
}

export interface SetUserList {
    type: actionTypes.SET_USER_LIST,
    payload: null | User[]
}
export interface GetDroneList {
    type: actionTypes.GET_DRONE_LIST,
    payload: null | User[]
}

export interface SetDroneList {
    type: actionTypes.SET_DRONE_LIST,
    payload: null | User[]
}

export interface SetCurrentUser {
    type: actionTypes.SET_CURRENT_USER,
    payload: null | User
}

export interface GetCurrentUser {
    type: actionTypes.GET_CURRENT_USER,
    payload: null | User
}

export interface RemoveCurrentUser {
    type: actionTypes.REMOVE_CURRENT_USER
}

// Type of actions
export type Action =
    SetPageTitle |
    SetLoader |
    ClearLoader |
    GetUserList |
    SetUserList |
    GetDroneList |
    SetDroneList |
    SetCurrentUser |
    GetCurrentUser |
    RemoveCurrentUser;