import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import reduxThunk from 'redux-thunk';

import App from './components/App';
import reducers from './reducers'
import * as actionTypes from './actions/actionTypes';

// Redux Devtools
const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Create Store
const store = createStore(
    reducers,
    composeEnhancers(applyMiddleware(reduxThunk))
);


// Setting initial data from local storage
const currentUser = localStorage.getItem('currentUser');
const userList = localStorage.getItem('userList');
const droneList = localStorage.getItem('droneList');

store.dispatch({ type: actionTypes.SET_CURRENT_USER, payload: currentUser ? JSON.parse(currentUser) : null });
store.dispatch({ type: actionTypes.SET_USER_LIST, payload: userList ? JSON.parse(userList) : null });
store.dispatch({ type: actionTypes.SET_DRONE_LIST, payload: droneList ? JSON.parse(droneList) : null });


ReactDOM.render(
    <Provider store={ store }>
        <App />
    </Provider>,
    document.querySelector('#root')
);