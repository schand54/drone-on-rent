import { User } from './components/features/login/interfaces';

export interface AppState {
    user: {
        userList: User[] | null,
        currentUser: User | null
    }
    form: any,
    drone: {
        droneList: any[] | null
    }
}